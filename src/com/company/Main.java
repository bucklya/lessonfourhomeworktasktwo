package com.company;

import java.util.Locale;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Bank[] Banks = new Bank[3];
        Banks[0] = new Bank("ПриватБанк",27.8f, 30.45f, 0.325f);
        Banks[1] = new Bank("ОщадБанк",27.6f, 29.25f, 0.38f);
        Banks[2] = new Bank("ПУМБ",28f, 30.5f, 0.34f);

        String USD = "USD";
        String EUR = "EUR";
        String RUB = "RUB";

        Scanner scan = new Scanner(System.in);

        System.out.println("Введите сумму которую вы хотите конвентировать: ");
        int amount = Integer.parseInt(scan.nextLine());

        System.out.println("Введите название банка с помощью кторого вы хотите провести операцию (ПриватБанк, ОщадБанк или ПУМБ): ");
        String nameBank = scan.nextLine();

        System.out.println("Введите валюту для конвертации (USD, EUR или RUB): ");
        String currency = scan.nextLine();

        for (Bank bank : Banks) {
            if (bank.name.equals(nameBank)){
                if(currency.equals(USD)){
                    System.out.println(String.format(Locale.US, "Ваша сумма в %s: %.2f", USD, amount/bank.USD));
                } else if (currency.equals(EUR)){
                    System.out.println(String.format(Locale.US, "Ваша сумма в %s: %.2f", EUR, amount/bank.EUR));
                } else if(currency.equals(RUB)){
                    System.out.println(String.format(Locale.US, "Ваша сумма в %s: %.2f", RUB, amount/bank.RUB));
                } else {
                    System.out.println("Введен неверный формат валюты!");
                }
            }
        }
    }
}
//        if (USD.equalsIgnoreCase(currency)) {
//        System.out.println(String.format(Locale.US, "You money in %s: %.2f", USD, amount / ));
//        } else if (EUR.equalsIgnoreCase(currency)) {
//        System.out.println(String.format(Locale.US, "You money in %s: %.2f", EUR, amount / ));
//        } else {
//        System.err.println("Can't convert to " + currency);
//        }
