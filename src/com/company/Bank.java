package com.company;

public class Bank {
    String name;
    float USD;
    float EUR;
    float RUB;

    public Bank (String name, float USD, float EUR, float RUB){
        this.name = name;
        this.USD = USD;
        this.EUR = EUR;
        this.RUB = RUB;
    }
}
